(ns xxx.core-test
  (:require [clojure.test :refer :all]
            [xxx.core :refer :all]))

(deftest kamil_nil_returns0
  (testing (is (= 0 (kamil nil)))))

(deftest kamil_emptyString_returns0
  (testing (is (= 0 (kamil "")))))

(deftest kamil_nonproblematicLettersOnly_returns1
  (testing (is (= 1 (kamil "mama")))))

(deftest kamil_letterT_returns2_becauseHeSometimesSaysTInsteadOfK
  (testing (is (= 2 (kamil "t")))))

(deftest kamil_letterK_returns1_becauseHeNeverSaysKInsteadOfT
  (testing (is (= 1 (kamil "k")))))

(deftest kamil_wordKok_returns2_becauseItCanMeanOnlyKok
         (testing (is (= 1 (kamil "kok")))))

(deftest kamil_wordKot_returns2_becauseItCanMeanKotOrKok
         (testing (is (= 2 (kamil "kot")))))

(deftest kamil_wordTot_returns4_becauseItCanMeanKokOrKotOtTokOrTot
         (testing (is (= 4 (kamil "tot")))))

(deftest kamil_letterD_returns2_becauseHeSometimesSaysDInsteadOfG
  (testing (is (= 2 (kamil "d")))))

(deftest kamil_letterG_returns1_becauseHeNeverSaysGInsteadOfD
  (testing (is (= 1 (kamil "g")))))

(deftest kamil_wordKogut_returns2_becauseItCanMeanKogutOrKoguk
         (testing (is (= 2 (kamil "kogut")))))

(deftest kamil_letterL_returns2_becauseHeSometimesSaysLInsteadOfR
  (testing (is (= 2 (kamil "l")))))

(deftest kamil_letterF_returns2_becauseHeSometimesSaysFInsteadOfR
  (testing (is (= 2 (kamil "f")))))

(deftest kamil_letterR_returns1
  (testing (is (= 1 (kamil "r")))))

(deftest kamil_filipek_returns4
  (testing (is (= 4 (kamil "filipek")))))

(deftest countAllOccurances_emptyString_returns0
         (testing (is (= 0 (countAllOccurances "" #{\a})))))

