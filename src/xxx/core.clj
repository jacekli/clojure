(ns xxx.core (:gen-class))
(require '[clojure.string :as string])

(def lettersWithOneVariant #{\t \d \l \f})

(defn exp [x n]
  (reduce * (repeat n x)))

(defn countAllOccurances [str letters]
  (count
    (filter
      (fn [char] (contains? letters char))
      (seq str))))

(defn kamil [input]
  (if (string/blank? input)
    0
    (exp 2 (countAllOccurances input lettersWithOneVariant))))


(defn -main
  [& args]
  (while true (println (kamil (read-line)))))
